#ifndef __CYCLEBUF_H
#define __CYCLEBUF_H

#include "stm32f7xx_hal.h"
#include "ltdc.h"

#define CYCLBUFF_SIZE 128

	typedef void (* tFunc)(void);
	tFunc pfFunc;
		
  typedef struct {
      volatile uint8_t cyclBuff[CYCLBUFF_SIZE];
      // volatile uint8_t *pRX_adr = cyclBuff;
		  // volatile uint8_t *pTX_adr = cyclBuff;
      // volatile uint64_t  rx_cycle = 0;
      // volatile uint64_t  tx_cycle = 0;
      volatile uint8_t *pRX_adr;
		  volatile uint8_t *pTX_adr;
      volatile uint64_t  rx_cycle;
      volatile uint64_t  tx_cycle;
			tFunc pfFunc;
  }t_CYCLEBUF_CTRL;


#endif /* __CYCLEBUF_H */