#ifndef __CYCLEBUF_H
#define __CYCLEBUF_H

#include "stm32f7xx_hal.h"
#include "ltdc.h"

#define CYCLBUFF_SIZE 128

void InitBuf(void);

	typedef void (* tFunc)(void);
		
  typedef struct {
      volatile uint8_t cyclBuff[CYCLBUFF_SIZE];
      volatile uint8_t *pRX_adr;
		  volatile uint8_t *pTX_adr;
      volatile uint64_t  rx_cycle;
      volatile uint64_t  tx_cycle;
			tFunc Init;
  }t_CYCLEBUF_CTRL;

#endif /* __CYCLEBUF_H */






	