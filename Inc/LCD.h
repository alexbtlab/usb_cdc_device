#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f7xx_hal.h"
#include "ltdc.h"
#include "CycleBuf.h"

#define BLUE  0x00001F
#define RED   0x00F800
#define GREEN 0x0007E0
#define BLACK 0x00000000
#define WHITE  0xFFFFFFFF
#define GRAY  0xA7A7A7
#define YELLOW 0xFFC0
#define YELLOW 0xFFC0

extern uint8_t volatile sizeDataRecieved;
extern uint8_t volatile sizeDataRecieved_inv;
extern t_CYCLEBUF_CTRL CycLeBuf;

#ifdef __cplusplus
extern "C" {
#endif

void Error_Handler(void);
void MX_LTDC_Init(void);
void viewDataTFT();
void viewMenu();
void TFT_DrawChar(uint16_t x, uint16_t y, const uint8_t c);
void TFT_DisplayString(uint16_t Xpos, uint16_t Ypos, const uint8_t *Text,
		Text_AlignModeTypedef Mode);
void TFT_SetFont(sFONT *fonts);
void TFT_SetTextColor(uint32_t color);
void TFT_SetBackColor(uint32_t color);
#ifdef __cplusplus
}
#endif



#endif /* __MAIN_H */
