#include "LCD.h"

volatile uint32_t RGB565_480x272[65280]={0x00000000};
LCD_DrawPropTypeDef lcdprop;
uint16_t X_SIZE =480;
uint16_t Y_SIZE =272;
const uint8_t *ch;
char str_to_print[128];
LTDC_HandleTypeDef hltdc;

void MX_LTDC_Init(void)
{

  /* USER CODE BEGIN LTDC_Init 0 */

  /* USER CODE END LTDC_Init 0 */

  LTDC_LayerCfgTypeDef pLayerCfg = {0};

  /* USER CODE BEGIN LTDC_Init 1 */

  /* USER CODE END LTDC_Init 1 */
  hltdc.Instance = LTDC;
  hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
  hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
  hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
  hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
  hltdc.Init.HorizontalSync = 40;
  hltdc.Init.VerticalSync = 9;
  hltdc.Init.AccumulatedHBP = 53;
  hltdc.Init.AccumulatedVBP = 11;
  hltdc.Init.AccumulatedActiveW = 533;
  hltdc.Init.AccumulatedActiveH = 283;
  hltdc.Init.TotalWidth = 565;
  hltdc.Init.TotalHeigh = 285;
  hltdc.Init.Backcolor.Blue = 0;
  hltdc.Init.Backcolor.Green = 0;
  hltdc.Init.Backcolor.Red = 0;
  if (HAL_LTDC_Init(&hltdc) != HAL_OK)
  {
    Error_Handler();
  }
  pLayerCfg.WindowX0 = 0;
  pLayerCfg.WindowX1 = 480;
  pLayerCfg.WindowY0 = 0;
  pLayerCfg.WindowY1 = 272;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB565;
  pLayerCfg.Alpha = 255;
  pLayerCfg.Alpha0 = 0;
  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_CA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_CA;
  pLayerCfg.FBStartAdress = 0;
  pLayerCfg.ImageWidth = 480;
  pLayerCfg.ImageHeight = 272;
  pLayerCfg.Backcolor.Blue = 0;
  pLayerCfg.Backcolor.Green = 0;
  pLayerCfg.Backcolor.Red = 0;
  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN LTDC_Init 2 */

  /* USER CODE END LTDC_Init 2 */

}
void viewDataTFT(){
        sprintf(str_to_print, "adrbufTX :%d" , (char *)CycLeBuf.pTX_adr);         // ????? ?? ????? ?????? ?????????? ?????? ????? ???? ??????????
        TFT_DisplayString(30, 190, (uint8_t *)str_to_print, LEFT_MODE);
        
        sprintf(str_to_print, "adrbufRX :%d" , CycLeBuf.pRX_adr);
        TFT_DisplayString(30, 210, (uint8_t *)str_to_print, LEFT_MODE);
        
        sprintf(str_to_print, "cyclerx_cycle :%d,%d" , (int)CycLeBuf.tx_cycle, (int)CycLeBuf.rx_cycle);
        TFT_DisplayString(30, 230, (uint8_t *)str_to_print, LEFT_MODE);

        sprintf(str_to_print, "sizeDataRecieved :%d" , (int)sizeDataRecieved);
        TFT_DisplayString(30, 250, (uint8_t *)str_to_print, LEFT_MODE);
}
void viewMenu(){
	HAL_LTDC_SetAddress(&hltdc,(uint32_t)&RGB565_480x272,0);
	TFT_FillScreen((uint32_t)BLUE);
	
	TFT_SetFont(&Font16);
	TFT_SetTextColor(BLUE);
	TFT_SetBackColor(WHITE);
	/*--------------------------------------------------------------------*/
	sprintf(str_to_print, "BasicTechLab 2021");
	TFT_DisplayString(290, 260, (uint8_t *)str_to_print, LEFT_MODE);
	
	TFT_SetFont(&Font16);
	TFT_SetTextColor(0xF80C);
	TFT_SetBackColor(BLUE);
	
	/*--------------------------------------------------------------------*/
	sprintf(str_to_print, " -*-----------USB CDC v1.0---------*- ");
	TFT_DisplayString(10, 10, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
	TFT_SetFont(&Font16);
	TFT_SetTextColor(WHITE);
	TFT_SetBackColor(BLUE);
	sprintf(str_to_print, " -*- Test USB-CDC Device          -*- ");
	TFT_DisplayString(10, 30, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
  sprintf(str_to_print, " -*- CDC VirtualComPort           -*- ");
	TFT_DisplayString(10, 50, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
  sprintf(str_to_print, " -*- Virtual Com Port COM6        -*-");
	TFT_DisplayString(10, 70, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
  sprintf(str_to_print, " -*- 115200 BaudeRate             -*-");
	TFT_DisplayString(10, 90, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
	sprintf(str_to_print, " -*--------------------------------*- ");
	TFT_DisplayString(10, 110, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
  sprintf(str_to_print, "  INFO: Transmit Data with 1 sec interval");
	TFT_DisplayString(10, 130, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
  sprintf(str_to_print, "  INFO: Waiting data from PC...");
	TFT_DisplayString(10, 150, (uint8_t *)str_to_print, LEFT_MODE);
	/*--------------------------------------------------------------------*/
	TFT_SetTextColor(0xFFC0);//(0xFFFD00);
	TFT_SetBackColor(BLUE);
	
  sprintf(str_to_print, "  INFO: Recieved Bytes - 0.1Kb");
	TFT_DisplayString(10, 170, (uint8_t *)str_to_print, LEFT_MODE);
}
void TFT_DrawChar(uint16_t x, uint16_t y, const uint8_t c)
{

	uint32_t i=0, j=0;
	uint16_t height, width;
	uint8_t offset;
	uint8_t *pchar;
	uint32_t line;
	ch = &lcdprop.pFont->table[(c-' ') * lcdprop.pFont->Height *
		((lcdprop.pFont->Width +7) / 8)];
	height = lcdprop.pFont->Height;
	width = lcdprop.pFont->Width;
	offset = 8 * ((width + 7)/8) - width;
	for(i = 0;i < height; i++)
	{
		pchar = ((uint8_t *)ch + (width + 7)/8 * i);
		switch((width + 7)/8)
		{
		case 1:
			line = pchar[0];
			break;
		case 2:
			line = (pchar[0]<<8) | pchar[1];
			break;
		case 3:
		default:
			line = (pchar[0]<<16) | (pchar[1]<<8) | pchar[2];
			break;
		}
		for(j = 0;j < width; j++)
		{
			if(line & (1 << (width - j + offset -1)))
			{
				TFT_DrawPixel((x+j), y, lcdprop.TextColor);
			}
			else
			{
				TFT_DrawPixel((x+j), y, lcdprop.BackColor);
			}
		}
		y++;
	}
}
void TFT_SetFont(sFONT *fonts){
	lcdprop.pFont=fonts;
}
void TFT_SetTextColor(uint32_t color){
	lcdprop.TextColor=color;
}
//----------------------------------------
void TFT_SetBackColor(uint32_t color){
	lcdprop.BackColor=color;
}

void TFT_DisplayString(uint16_t Xpos, uint16_t Ypos, const uint8_t *Text,
		Text_AlignModeTypedef Mode)

{
	uint16_t ref_column = 1, i = 0;
	uint32_t size = 0, xsize = 0;
	const uint8_t *ptr = Text;
	while(*ptr++) size++ ;
	xsize = (X_SIZE/lcdprop.pFont->Width);
	switch (Mode)
	{
		case CENTER_MODE:
		{
			ref_column = Xpos + ((xsize-size) * lcdprop.pFont->Width) / 2;
			break;
		}
		case LEFT_MODE:
		{
			ref_column = Xpos;
			break;
		}
		case RIGHT_MODE:
		{
			ref_column = Xpos + ((xsize-size) * lcdprop.pFont->Width);
			break;
		}
		default:
		{
			ref_column = Xpos;
			break;
		}
	}
	if((ref_column < 1) || (ref_column >= 0x8000))
	{
		ref_column = 1;
	}
	while ((*Text != 0) & (((X_SIZE - (i*lcdprop.pFont->Width)) & 0xFFFF) >=
			lcdprop.pFont->Width))
	{
		TFT_DrawChar(ref_column, Ypos, *Text);
		ref_column += lcdprop.pFont->Width;
		Text++;
		i++;
	}
}