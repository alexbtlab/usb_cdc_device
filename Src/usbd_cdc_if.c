/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : usbd_cdc_if.c
  * @version        : v1.0_Cube
  * @brief          : Usb device for Virtual Com Port.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc_if.h"
#include "main.h"
/* USER CODE BEGIN INCLUDE */
 
/* USER CODE END INCLUDE */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @brief Usb device library.
  * @{
  */

/** @addtogroup USBD_CDC_IF
  * @{
  */

/** @defgroup USBD_CDC_IF_Private_TypesDefinitions USBD_CDC_IF_Private_TypesDefinitions
  * @brief Private types.
  * @{
  */

/* USER CODE BEGIN PRIVATE_TYPES */

/* USER CODE END PRIVATE_TYPES */

/**
  * @}
  */

/** @defgroup USBD_CDC_IF_Private_Defines USBD_CDC_IF_Private_Defines
  * @brief Private defines.
  * @{
  */

/* USER CODE BEGIN PRIVATE_DEFINES */
/* USER CODE END PRIVATE_DEFINES */

/**
  * @}
  */

/** @defgroup USBD_CDC_IF_Private_Macros USBD_CDC_IF_Private_Macros
  * @brief Private macros.
  * @{
  */

/* USER CODE BEGIN PRIVATE_MACRO */

/* USER CODE END PRIVATE_MACRO */

/**
  * @}
  */

/** @defgroup USBD_CDC_IF_Private_Variables USBD_CDC_IF_Private_Variables
  * @brief Private variables.
  * @{
  */
/* Create buffer for reception and transmission           */
/* It's up to user to redefine and/or remove those define */
/** Received data over USB are stored in this buffer      */
uint8_t UserRxBufferFS[APP_RX_DATA_SIZE];

/** Data to send over USB CDC are stored in this buffer   */
uint8_t UserTxBufferFS[APP_TX_DATA_SIZE];

/* USER CODE BEGIN PRIVATE_VARIABLES */

/* USER CODE END PRIVATE_VARIABLES */

/**
  * @}
  */
extern  t_CYCLEBUF_CTRL CycLeBuf; 
/** @defgroup USBD_CDC_IF_Exported_Variables USBD_CDC_IF_Exported_Variables
  * @brief Public variables.
  * @{
  */

extern USBD_HandleTypeDef hUsbDeviceFS;

/* USER CODE BEGIN EXPORTED_VARIABLES */

/* USER CODE END EXPORTED_VARIABLES */

/**
  * @}
  */

/** @defgroup USBD_CDC_IF_Private_FunctionPrototypes USBD_CDC_IF_Private_FunctionPrototypes
  * @brief Private functions declaration.
  * @{
  */

static int8_t CDC_Init_FS(void);
static int8_t CDC_DeInit_FS(void);
static int8_t CDC_Control_FS(uint8_t cmd, uint8_t* pbuf, uint16_t length);
static int8_t CDC_Receive_FS(uint8_t* pbuf, uint32_t *Len);
static int8_t CDC_TransmitCplt_FS(uint8_t *pbuf, uint32_t *Len, uint8_t epnum);

/* USER CODE BEGIN PRIVATE_FUNCTIONS_DECLARATION */

/* USER CODE END PRIVATE_FUNCTIONS_DECLARATION */

/**
  * @}
  */

USBD_CDC_ItfTypeDef USBD_Interface_fops_FS =
{
  CDC_Init_FS,
  CDC_DeInit_FS,
  CDC_Control_FS,
  CDC_Receive_FS,
  CDC_TransmitCplt_FS
};

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Initializes the CDC media low layer over the FS USB IP
  * @retval USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Init_FS(void)
{
  /* USER CODE BEGIN 3 */
  /* Set Application Buffers */
  USBD_CDC_SetTxBuffer(&hUsbDeviceFS, UserTxBufferFS, 0);
  USBD_CDC_SetRxBuffer(&hUsbDeviceFS, UserRxBufferFS);
  return (USBD_OK);
  /* USER CODE END 3 */
}

/**
  * @brief  DeInitializes the CDC media low layer
  * @retval USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_DeInit_FS(void)
{
  /* USER CODE BEGIN 4 */
  return (USBD_OK);
  /* USER CODE END 4 */
}

/**
  * @brief  Manage the CDC class requests
  * @param  cmd: Command code
  * @param  pbuf: Buffer containing command data (request parameters)
  * @param  length: Number of data to be sent (in bytes)
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Control_FS(uint8_t cmd, uint8_t* pbuf, uint16_t length)
{
  /* USER CODE BEGIN 5 */
  switch(cmd)
  {
    case CDC_SEND_ENCAPSULATED_COMMAND:

    break;

    case CDC_GET_ENCAPSULATED_RESPONSE:

    break;

    case CDC_SET_COMM_FEATURE:

    break;

    case CDC_GET_COMM_FEATURE:

    break;

    case CDC_CLEAR_COMM_FEATURE:

    break;

  /*******************************************************************************/
  /* Line Coding Structure                                                       */
  /*-----------------------------------------------------------------------------*/
  /* Offset | Field       | Size | Value  | Description                          */
  /* 0      | dwDTERate   |   4  | Number |Data terminal rate, in bits per second*/
  /* 4      | bCharFormat |   1  | Number | Stop bits                            */
  /*                                        0 - 1 Stop bit                       */
  /*                                        1 - 1.5 Stop bits                    */
  /*                                        2 - 2 Stop bits                      */
  /* 5      | bParityType |  1   | Number | Parity                               */
  /*                                        0 - None                             */
  /*                                        1 - Odd                              */
  /*                                        2 - Even                             */
  /*                                        3 - Mark                             */
  /*                                        4 - Space                            */
  /* 6      | bDataBits  |   1   | Number Data bits (5, 6, 7, 8 or 16).          */
  /*******************************************************************************/
    case CDC_SET_LINE_CODING:

    break;

    case CDC_GET_LINE_CODING:

    break;

    case CDC_SET_CONTROL_LINE_STATE:

    break;

    case CDC_SEND_BREAK:

    break;

  default:
    break;
  }

  return (USBD_OK);
  /* USER CODE END 5 */
}

/**
  * @brief  Data received over USB OUT endpoint are sent over CDC interface
  *         through this function.
  *
  *         @note
  *         This function will issue a NAK packet on any OUT packet received on
  *         USB endpoint until exiting this function. If you exit this function
  *         before transfer is complete on CDC interface (ie. using DMA controller)
  *         it will result in receiving more data while previous ones are still
  *         not sent.
  *
  * @param  Buf: Buffer of data to be received
  * @param  Len: Number of data received (in bytes)
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */



/**
  * @brief  Пробуем циклический буфер 
  * -------------------------------------------------------------------------------------------------------------
  *  - Приняли 6 байт. Указатели принятых байт и переданных находятся в одном периоде циклического буфера 
  *              p_tx(1)     p_rx(1)   
  *              |           |
  *      |x|x|x|x|o|o|o|o|o|o|-|-|-|-|   ---   |-|-|
  *       0 1 2 3 . . 6                           1024
  * -------------------------------------------------------------------------------------------------------------
  *  - Приняли 3 байта. Указатель переданных байт отстает от указателя принятых байт на один период. 
  *    т.е. не успеваем передавать принятые байты
  *    Если при следующем приеме принятых байт будет больше чем осталось в буфере(что бы не догнать указатель передачи который еле движется) то 
  *    необходимо как-то остановить USB Host  
  *                p_rx(2)     p_tx(1)
  *                | -остаток- |
  *      |o|o|o|o|o|x|x|x|x|x|x|-|-|-|   ---   |-|-|
  *       0 1 2 3 . . 6                           1024
  * -------------------------------------------------------------------------------------------------------------
  *   (x) - где X - номер периода циклического буфера в котором нахядятся указатели
  *   "x" - переданные байты
  *   "0" - принятые байты 
  *   "-" - пространство массива для приема данных   
  * -------------------------------------------------------------------------------------------------------------
  * @param  Buf: Buffer of data to be received
  * @param  Len: Number of data received (in bytes)
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Receive_FS(uint8_t* Buf, uint32_t *Len){
	
  static volatile uint8_t  p_RX = 0;            // Относительный "УКАЗАТЕЛЬ ПРИЕМА" циклического буфера для записи принятых данных. т.е. (приняли 35 bytes -> p_RX = 35)
  
        if(CycLeBuf.rx_cycle == CycLeBuf.tx_cycle){   // Если в момент приема указатели p_rx p_tx находятся на одном обороте (периоде) буфера
                  if(     !((CYCLBUFF_SIZE - (int)(CycLeBuf.pRX_adr - CycLeBuf.cyclBuff)) >= *Len)){    //      Если остаток свободной памяти в циклическом буфере НЕ достаточен для принятия данных то ->                                                                         
                                                                                                        // В случае отсутствия свободной памяти в циклическом буфере (в случая КБ-не догнать указатель УКАЗАТЕЛЬ ПЕРЕДАЧИ) памяти   
                      CycLeBuf.pRX_adr = CycLeBuf.cyclBuff;                                             // то необходимо класть принятые данные в начало буфера и переходим на следующий оборот 
                      CycLeBuf.pTX_adr = CycLeBuf.cyclBuff;                                             // и устанавливаем "УКАЗАТЕЛЬ ПРИЕМА" на начало кольцевого массива т.к. после приема передавать будем из начала массива
                      CycLeBuf.rx_cycle++;					                                                    //      Прошли оборот начинаем писать в е=начало и инкркементирум число оборотов   
                  }                                                          
                  p_RX = CycLeBuf.pRX_adr - CycLeBuf.cyclBuff;                                          //      Определяем относительный адресс в массиве для последующей записи в него принятых данных         
                  strncpy((char *)&CycLeBuf.cyclBuff[p_RX], (char *)Buf, *Len);                         //      Производим запись данных в позицию кольцевого буфера соответсвующей текущему "УКАЗАТЕЛЬ ПРИЕМА"
                  CycLeBuf.pRX_adr = CycLeBuf.pRX_adr + *Len;                                           //      Смещаем "УКАЗАТЕЛЬ ПРИЕМА" на количество принятых байт
        }
        /*----------------------------------------------*/
        if(CycLeBuf.rx_cycle > CycLeBuf.tx_cycle){                                                                // Если "УКАЗАТЕЛЬ ПРИЕМА" уже прошел оборот и догоняет "УКАЗАТЕЛЬ ПЕРЕДАЧИ" 
                  p_RX = CycLeBuf.pRX_adr - CycLeBuf.cyclBuff;                                                    //    Если есть место чтобы принять данные и 
                  if(   (int)(CycLeBuf.pTX_adr - CycLeBuf.pRX_adr)  >= *Len)                                      // НЕ ЗАТЕРЕТЬ данные (т.е. опередить "УКАЗАТЕЛЬ ПЕРЕДАЧИ"
                          strncpy((char *)&CycLeBuf.cyclBuff[p_RX], (char *)Buf, *Len);                           // (ПЕРЕДАЧА не успеват за ПРИЕМОМ) )Записываем данные в циклицеский буфер 
                  else                                                                                            //      "УКАЗАТЕЛЬ ПРИЕМА" догал по кругу "УКАЗАТЕЛЬ ПЕРЕДАЧИ" и 
                          TFT_DisplayString(10, 250, (uint8_t *)"Error:001", LEFT_MODE);                          // необходимо останавливать поток данных USB HOST стороны  
                  
        }
        USBD_CDC_SetRxBuffer(&hUsbDeviceFS, &Buf[0]);
        USBD_CDC_ReceivePacket(&hUsbDeviceFS);
        return (USBD_OK);
}
/**
  * @brief  CDC_Transmit_FS
  *         Data to send over USB IN endpoint are sent over CDC interface
  *         through this function.
  *         @note
  *
  *
  * @param  Buf: Buffer of data to be sent
  * @param  Len: Number of data to be sent (in bytes)
  * @retval USBD_OK if all operations are OK else USBD_FAIL or USBD_BUSY
  */
uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len)
{
  uint8_t result = USBD_OK;
  /* USER CODE BEGIN 7 */
  USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;
  if (hcdc->TxState != 0){
    return USBD_BUSY;
  }
  USBD_CDC_SetTxBuffer(&hUsbDeviceFS, Buf, Len);
  result = USBD_CDC_TransmitPacket(&hUsbDeviceFS);
  /* USER CODE END 7 */
  return result;
}

/**
  * @brief  CDC_TransmitCplt_FS
  *         Data transmited callback
  *
  *         @note
  *         This function is IN transfer complete callback used to inform user that
  *         the submitted Data is successfully sent over USB.
  *
  * @param  Buf: Buffer of data to be received
  * @param  Len: Number of data received (in bytes)
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_TransmitCplt_FS(uint8_t *Buf, uint32_t *Len, uint8_t epnum)
{
  uint8_t result = USBD_OK;
  /* USER CODE BEGIN 13 */
  UNUSED(Buf);
  UNUSED(Len);
  UNUSED(epnum);
  /* USER CODE END 13 */
  return result;
}

/* USER CODE BEGIN PRIVATE_FUNCTIONS_IMPLEMENTATION */

/* USER CODE END PRIVATE_FUNCTIONS_IMPLEMENTATION */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
